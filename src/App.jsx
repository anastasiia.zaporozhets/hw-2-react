import Button from "./components/Button/Button.jsx";
import ModalImage from "./components/ModalComponents/ModalImage/ModalImage.jsx";
import ModalText from "./components/ModalComponents/ModalText/ModalText.jsx";
import {useState, useEffect} from "react";
import Header from "./components/HeaderComponents/Header/Header.jsx";
import Slider from "./components/Slider/Slider.jsx";
import ProductsList from "./components/ProductsComponents/ProductList/ProductList.jsx";

function App() {

    const [modal, setModal] = useState({
        isOpen: false,
        modalType: null,
        data: {}
    });

    const [data, setData] = useState([]);

    useEffect(() => {
        fetch("products.json")
            .then(response => response.json())
            .then(data => setData(data))

    }, []);

    const [basket, setBasket] = useState(() => {
        const basket = JSON.parse(localStorage.getItem('basket')) || []
        return basket.length;
    })

    function addToLocalStorageBasket() {
        const basket = JSON.parse(localStorage.getItem('basket')) || []
        if (!basket.includes(modal.data.id)) {
            basket.push(modal.data.id)
            setBasket(basket.length)
            localStorage.setItem('basket', JSON.stringify(basket))
        }}

    const [favorite, setFavorite] = useState(() => {
        const favorite = JSON.parse(localStorage.getItem("favorite")) || [];
        return favorite.length;
    });

    function addToLocalStorageFavorite(id) {
        const favorite = JSON.parse(localStorage.getItem("favorite")) || [];
        if (!favorite.includes(id)) {
            favorite.push(id)
            setFavorite(favorite.length);
            localStorage.setItem('favorite', JSON.stringify(favorite));
        }}

    function openModalHandler(modalType, data = {}) {
        setModal({
            isOpen: true,
            modalType: modalType,
            data: data
        })}

    function closeModalHandler() {
        setModal({
            isOpen: false,
            modalType: null,
            data: {}
        })}

    return (
        <>
            <Header basketCounter={basket} favoriteCounter={favorite}/>
            <Slider/>
            <ProductsList items={data} onBuyClick={(itemData) => openModalHandler("image", itemData)}
                          clickStar={addToLocalStorageFavorite}/>

            {modal.isOpen && modal.modalType === "image" && (
                <ModalImage closeModal={closeModalHandler} data={modal.data} addToLocalStorage={addToLocalStorageBasket}/>
            )}

        </>
    )
}

export default App;