import "./ModalWrapper.scss"

function ModalWrapper({children}) {

    return (
        <div className="modal-wrapper">{children}</div>
    )
}

export default ModalWrapper;