import {useEffect} from "react";
import photo from "../../../assets/photo-img.jpeg"
import Modal from "../Modal/Modal.jsx";
import ModalWrapper from "../ModalWrapper/ModalWrapper.jsx";
import ModalHeader from "../ModalHeader/ModalHeader.jsx";
import ModalClose from "../ModalClose/ModalClose.jsx";
import ModalBody from "../ModalBody/ModalBody.jsx";
import ModalFooter from "../ModalFooter/ModalFooter.jsx";
import Button from "../../Button/Button.jsx";

function ModalImage({closeModal, data, addToLocalStorage}) {

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (!event.target.closest('.modal')) {
                closeModal();
            }
        };

        document.addEventListener('mousedown', handleOutsideClick);

        return () => {
            document.removeEventListener('mousedown', handleOutsideClick);
        };
    }, [closeModal]);

    return (
        <>
            <ModalWrapper>
                <Modal className="modal modal__modal-image">
                    <ModalHeader>
                        <ModalClose onClick={closeModal}/>
                    </ModalHeader>
                    <ModalBody>
                        <img className="modal-body__img-top" src={data.imageUrl} alt="img"/>
                        <h1 className="modal-body__title modal-body__title__modal-image">{data.name}</h1>
                        <p className="modal-body__price">Ціна: {data.price} грн</p>
                        <p className="modal-body__text ">ДОДАТИ ТОВАР ДО КОШИКА? </p>
                    </ModalBody>
                    <ModalFooter className="modal-footer modal-footer__modal-image">
                        <Button type="button" className="btn-modal btn-modal__left-btn" onClick={closeModal} >НІ, ВІДМІНИТИ</Button>
                        <Button type="button" className="btn-modal btn-modal__right-btn"  onClick={()=>{
                            addToLocalStorage();
                            closeModal();
                        }}>ТАК, ДОДАТИ В КОШИК</Button>
                    </ModalFooter>

                </Modal>
            </ModalWrapper>

        </>
    )

}

export default ModalImage;