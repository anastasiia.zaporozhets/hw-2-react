import "./HeaderNoteBlock.scss"

function HeaderNoteBlock({className,children}) {
    return (
        <div className={className}>{children}</div>
    )
}

export default HeaderNoteBlock;