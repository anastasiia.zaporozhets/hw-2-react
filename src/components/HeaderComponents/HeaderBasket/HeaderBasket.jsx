import "./HeaderBasket.scss"
function HeaderBasket({className,children}) {
    return(

        <div className={className}>{children}</div>
    )
}

export default HeaderBasket;