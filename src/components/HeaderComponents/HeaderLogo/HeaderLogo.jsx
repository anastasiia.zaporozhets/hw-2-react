import "./HeaderLogo.scss"

function HeaderLogo({className, children}) {
    return(
        // <div className="wrapper-logo">
        //     <a href="#" className="wrapper-logo__logo-link">
        //         <h1 className="wrapper-logo__logo-link__logo">MilitaryStore</h1>
        //     </a>
        //     <p className="wrapper-logo__text">Військове спорядження та одяг</p>
        // </div>
        <div className={className}>{children}</div>
    )
}

export default HeaderLogo;