import "./HeaderNavMenu.scss"


function HeaderNavMenu({className, children}) {
    return (
        <nav className={className}>{children}</nav>
    )
}

export default HeaderNavMenu;