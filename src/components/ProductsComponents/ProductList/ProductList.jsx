import PropTypes from "prop-types";
import "./ProductList.scss"
import ProductCard from "../ProductCard/ProductCard.jsx";


function ProductsList({ items, onBuyClick , clickStar}) {
    return (
        <section className="section-product-list">
            <h1 className="section-product-list__title">Тактичний одяг для чоловіків</h1>
            <div className="section-product-list__wrapper">
                {items.map((item, index) => (
                    <ProductCard key={index} item={item} onBuyClick={onBuyClick} clickStar = {clickStar} />
                ))}
            </div>
        </section>
    );
}

ProductsList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.string.isRequired
    })).isRequired,
    onBuyClick: PropTypes.func.isRequired,
    clickStar: PropTypes.func.isRequired,
};


export default ProductsList;